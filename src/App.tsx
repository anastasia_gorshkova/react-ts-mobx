import React from 'react';
import { Router } from 'react-router-dom';
import { ThemeProvider } from 'emotion-theming';
import { createBrowserHistory } from 'history';
import Routing from '@/routing';
import { ProductStore } from '@/store/ProductStore';
import { Provider } from 'mobx-react';
import { theme, GlobalStyles } from './theme';

const history = createBrowserHistory();
history.listen(() => {
  window.scrollTo(0, 0);
});

const productStore = new ProductStore();

export const App: React.FC = () => (
  <Provider productStore={productStore}>
    <Router history={history}>
      <ThemeProvider theme={theme}>
        <GlobalStyles />
        <Routing />
      </ThemeProvider>
    </Router>
  </Provider>
);
