import React, { ButtonHTMLAttributes } from 'react';
import styled from '@/theme';
import { Preloader } from './Icon';

export const Wrapper = styled.button`
  border-radius: 8px;
  padding: 8px 40px;
  height: 40px;
  font-size: 18px;
  text-transform: capitalize;
  cursor: pointer;
  outline: none;
  overflow: hidden;
  position: relative;
  background: transparent;
  border: none;
  box-shadow: none;
  color: ${({ theme }) => theme.colors.primary};
  @media only screen and (max-width: 600px) {
    padding: 8px 16px;
  }
  & span {
    position: relative;
    z-index: 1;
  }
  &::after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    height: 100%;
    width: 100%;
    background: ${({ theme }) => theme.colors.orange};
    display: block;
    transform: scaleX(0.3);
    opacity: 0;
    transition: all 0.3s;
  }
  &:hover::after {
    opacity: 1;
    transform: scaleX(1);
    transition: transform 0.6s cubic-bezier(0.08, 0.35, 0.13, 1.02), opacity, color;
  }
  &:hover {
    color: ${({ theme }) => theme.colors.bg};
  }
`;

type TProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  isLoading?: boolean;
  disabled?: boolean;
};

export const Basic: React.FC<TProps> = ({ children, isLoading, disabled, ...props }) => (
  <Wrapper {...props} disabled={isLoading || disabled}>
    <span>{isLoading ? <Preloader /> : children}</span>
  </Wrapper>
);

export const Primary = styled(Basic)`
  background: ${({ theme }) => theme.colors.primary};
  border: none;
  box-shadow: 0px 0px 8px ${({ theme }) => theme.colors.primary};
  color: ${({ theme }) => theme.colors.bg};
  &:hover {
    box-shadow: none;
    color: ${({ theme }) => theme.colors.bg};
  }
  &:disabled {
    background: ${({ theme }) => theme.colors.greyText7};
    box-shadow: none;
    cursor: not-allowed;
    &:hover::after {
      opacity: 0;
    }
  }
`;

export const Secondary = styled(Basic)`
  background: transparent;
  border: 1px solid ${({ theme }) => theme.colors.primary};
  box-shadow: none;
  color: ${({ theme }) => theme.colors.primary};
  &:hover {
    border: 1px solid ${({ theme }) => theme.colors.orange};
    color: ${({ theme }) => theme.colors.bg};
  }
  &:disabled {
    border-color: ${({ theme }) => theme.colors.greyText7};
    color: ${({ theme }) => theme.colors.greyText7};
    cursor: not-allowed;
    &:hover::after {
      opacity: 0;
    }
  }
`;
