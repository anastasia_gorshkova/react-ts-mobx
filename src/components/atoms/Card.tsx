import styled from '@/theme';

type TProps = {
  maxWidth?: number;
};

export const Card = styled.div<TProps>`
  max-width: ${({ maxWidth }) => `${maxWidth}px` || 'auto'};
  width: 100%;
  margin: auto;
  padding: 72px;
  background-color: ${({ theme }) => theme.colors.bg};
  box-shadow: 0px 0px 16px ${({ theme }) => theme.colors.bg1};
  border-radius: 8px;
`;
