import React from 'react';
import { Link } from 'react-router-dom';
import styled from '@/theme';

const StyledLink = styled.a`
  text-decoration: none;
  color: ${({ theme }) => theme.colors.purpleText7};
  cursor: pointer;

  &:hover {
    text-decoration: underline;
  }
`;

type TProps = {
  href: string;
  className?: string;
};

export const ExternalLink: React.FC<TProps> = ({ children, href, className = '' }) => (
  <StyledLink href={href} target="_blank" rel="noopener noreferrer" className={className}>
    {children}
  </StyledLink>
);

export const InnerLink = styled(Link)`
  text-decoration: none;
  color: ${({ theme }) => theme.colors.purpleText7};
  cursor: pointer;

  &:hover {
    text-decoration: underline;
  }
`;
