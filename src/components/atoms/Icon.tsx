import React from 'react';

type TProps = {
  width?: number;
  height?: number;
  className?: string;
};

export const withIcon = (Child: React.FC, vb = [0, 0, 24, 24]): React.FC<TProps> => ({
  width = vb[2] - vb[0],
  height = vb[3] - vb[1],
  className = '',
}) => (
  <svg viewBox={`${vb[0]} ${vb[1]} ${vb[2]} ${vb[3]}`} width={width} height={height} className={className}>
    <Child />
  </svg>
);

/* eslint-disable max-len */

export const Logo = withIcon(
  () => (
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M0 61.3709H32V48.7206L3.8529 44.5357L32 45.1388V5.48294C32 2.46674 29.5311 0 26.5156 0C23.5008 0 21.0341 2.46674 21.0341 5.48294V22.6414H10.9674V5.48294C10.9674 2.46674 8.49988 0 5.48368 0C2.46821 0 0 2.46674 0 5.48294V61.3709ZM21.1671 35.7647C21.5704 35.7647 21.8972 36.0916 21.8972 36.4957C21.8972 36.9004 21.5704 37.2258 21.1671 37.2258C20.763 37.2258 20.4369 36.9004 20.4369 36.4957C20.4369 36.0916 20.763 35.7647 21.1671 35.7647ZM11.3016 34.6474C12.3234 34.6474 13.1513 35.4753 13.1513 36.4957C13.1513 37.516 12.3234 38.3453 11.3016 38.3453C10.282 38.3453 9.45338 37.516 9.45338 36.4957C9.45338 35.4753 10.282 34.6474 11.3016 34.6474Z"
      fill="currentColor"
    />
  ),
  [0, 0, 32, 62],
);

export const Menu = withIcon(() => <path fill="currentColor" d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z" />);

export const Close = withIcon(() => (
  <path
    fill="currentColor"
    d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z"
  />
));

export const ChevronLeft = withIcon(() => (
  <path fill="currentColor" d="M15.41,16.58L10.83,12L15.41,7.41L14,6L8,12L14,18L15.41,16.58Z" />
));

export const ChevronRight = withIcon(() => (
  <path fill="currentColor" d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" />
));

export const ChevronDown = withIcon(() => (
  <path fill="currentColor" d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z" />
));

export const ChevronUp = withIcon(() => (
  <path fill="currentColor" d="M7.41,15.41L12,10.83L16.59,15.41L18,14L12,8L6,14L7.41,15.41Z" />
));

export const Error = withIcon(() => (
  <path
    fill="currentColor"
    d="M11,15H13V17H11V15M11,7H13V13H11V7M12,2C6.47,2 2,6.5 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20Z"
  />
));

export const Success = withIcon(() => (
  <path fill="currentColor" d="M9,20.42L2.79,14.21L5.62,11.38L9,14.77L18.88,4.88L21.71,7.71L9,20.42Z" />
));

export const ImageFolder = withIcon(() => (
  <path fill="currentColor" d="M4,5H7L9,3H15L17,5H20A2,2 0 0,1 22,7V19A2,2 0 0,1 20,21H4A2,2 0 0,1 2,19V7A2,2 0 0,1 4,5M13.09,9.45L11.05,12.18L12.6,14.25L11.73,14.91L9.27,11.64L6,16H18L13.09,9.45Z" />
));

export const CardLayout = withIcon(() => (
  <path fill="currentColor" d="M18 2H6C4.89 2 4 2.9 4 4V20C4 21.11 4.89 22 6 22H18C19.11 22 20 21.11 20 20V4C20 2.9 19.11 2 18 2M18 20H6V16H18V20M18 8H6V4H18V8Z" />
));

export const Cards = withIcon(() => (
  <path fill="currentColor" d="M21.47,4.35L20.13,3.79V12.82L22.56,6.96C22.97,5.94 22.5,4.77 21.47,4.35M1.97,8.05L6.93,20C7.24,20.77 7.97,21.24 8.74,21.26C9,21.26 9.27,21.21 9.53,21.1L16.9,18.05C17.65,17.74 18.11,17 18.13,16.26C18.14,16 18.09,15.71 18,15.45L13,3.5C12.71,2.73 11.97,2.26 11.19,2.25C10.93,2.25 10.67,2.31 10.42,2.4L3.06,5.45C2.04,5.87 1.55,7.04 1.97,8.05M18.12,4.25A2,2 0 0,0 16.12,2.25H14.67L18.12,10.59" />
));

export const Preloader = withIcon(
  () => (
    <>
      <rect x="0" y="13" width="4" height="5" fill="currentColor">
        <animate
          attributeName="height"
          attributeType="XML"
          values="5;21;5"
          begin="0s"
          dur="0.6s"
          repeatCount="indefinite"
        />
        <animate
          attributeName="y"
          attributeType="XML"
          values="13; 5; 13"
          begin="0s"
          dur="0.6s"
          repeatCount="indefinite"
        />
      </rect>
      <rect x="10" y="13" width="4" height="5" fill="currentColor">
        <animate
          attributeName="height"
          attributeType="XML"
          values="5;21;5"
          begin="0.15s"
          dur="0.6s"
          repeatCount="indefinite"
        />
        <animate
          attributeName="y"
          attributeType="XML"
          values="13; 5; 13"
          begin="0.15s"
          dur="0.6s"
          repeatCount="indefinite"
        />
      </rect>
      <rect x="20" y="13" width="4" height="5" fill="currentColor">
        <animate
          attributeName="height"
          attributeType="XML"
          values="5;21;5"
          begin="0.3s"
          dur="0.6s"
          repeatCount="indefinite"
        />
        <animate
          attributeName="y"
          attributeType="XML"
          values="13; 5; 13"
          begin="0.3s"
          dur="0.6s"
          repeatCount="indefinite"
        />
      </rect>
    </>
  ),
  [0, 0, 24, 30],
);

/* eslint-enable max-len */
