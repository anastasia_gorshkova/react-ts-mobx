import styled from '@/theme';

export const Input = styled.input`
  border-radius: 8px;
  padding: 8px;
  height: 40px;
  font-size: 18px;
  outline: none;
  background: ${({ theme }) => theme.colors.bg};
  border: 1px solid ${({ theme }) => theme.colors.primary};
  color: ${({ theme }) => theme.colors.primary};
  &::placeholder {
    color: ${({ theme }) => theme.colors.greyText3};
  }
`;
