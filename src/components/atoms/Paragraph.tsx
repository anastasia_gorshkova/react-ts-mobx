import styled from '@/theme';

type TProps = {
  fontSize?: number;
  textAlign?: 'left' | 'right' | 'center';
  fontWeight?: '300' | '400' | '700';
};

export const Paragraph = styled.p<TProps>`
  margin: 0 auto;
  font-size: ${({ fontSize }) => fontSize || 18}px;
  text-align: ${({ textAlign }) => textAlign || 'left'};
  font-weight: ${({ fontWeight }) => fontWeight || '400'};
  color: ${({ theme }) => theme.colors.yellowText14Light};
  @media only screen and (max-width: 600px) {
    font-size: 18px;
  }
`;
