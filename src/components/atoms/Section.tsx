import styled from '@/theme';

export const Section = styled.div`
  max-width: ${({ theme }) => theme.width};
  width: 100%;
  padding: 0 72px;
  margin: auto;
`;
