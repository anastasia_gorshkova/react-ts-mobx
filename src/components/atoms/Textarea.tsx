import styled from '@/theme';

export const Textarea = styled.textarea`
  border-radius: 8px;
  padding: 8px;
  min-height: 40px;
  font-size: 18px;
  outline: none;
  background: ${({ theme }) => theme.colors.bg};
  border: 1px solid ${({ theme }) => theme.colors.primary};
  color: ${({ theme }) => theme.colors.primary};
  resize: none;
  width: 100%;
  &::placeholder {
    color: ${({ theme }) => theme.colors.greyText3};
  }
`;
