import styled from '@/theme';

export const Title = styled.h2`
  font-family: 'Roboto Condensed', sans-serif;
  font-weight: 700;
  line-height: 1;
  margin: 0 0 36px;
  font-size: 36px;
  color: ${({ theme }) => theme.colors.primary};
  text-align: 'left';
`;
