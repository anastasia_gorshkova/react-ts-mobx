import React from 'react';
import styled from '@/theme';
import { Section } from '@/components/atoms';

export const FooterSection = styled(Section)`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding-top: 16px;
  padding-bottom: 16px;
  font-size: 14px;
  line-height: 16px;
  color: ${({ theme }) => theme.colors.greyText7};
`;

export const Footer: React.FC = () => (
  <FooterSection>
    <div>© 2020</div>
  </FooterSection>
);
