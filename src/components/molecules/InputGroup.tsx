import React from 'react';
import styled from '@/theme';
import { Input, Button } from '@/components/atoms';

const InputWrapper = styled.div`
  display: flex;
`;

const InputButton = styled(Button.Primary)`
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
  box-shadow: none;
`;

const InputInput = styled(Input)`
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
  border-right: none;
  flex: 1;
`;

type TProps = {
  text: string;
  className?: string;
};

export const InputGroup: React.FC<TProps> = ({ text, className, ...props }) => (
  <InputWrapper className={className}>
    <InputInput {...props} />
    <InputButton>{text}</InputButton>
  </InputWrapper>
);
