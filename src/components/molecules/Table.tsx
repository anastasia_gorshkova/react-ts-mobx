import React, { useCallback, useState, useMemo } from 'react';
import styled from '@/theme';
import { Icon } from '@/components/atoms';

type TColumn = {
  field: string;
  header: string;
  value?: (value: any, item: any) => React.ReactNode;
  width?: number;
};

type TRow = {
  item: any;
  columns: TColumn[];
  onRowClick?: (id: string | number) => void;
  rowKey: string;
};

type THead = {
  column: TColumn;
  sortState: number;
  changeSorting: (field: string) => void;
};

type TProps = {
  items: any[];
  columns: TColumn[];
  rowKey?: string;
  onRowClick?: (id: string | number) => void;
};

const tableCellPadding = '12px 10px';

const InnerTable = styled.table`
  width: '100%';
`;

const Th = styled.th`
  border-bottom: 1px solid ${({ theme }) => theme.colors.orange};
  padding: ${tableCellPadding};
  font-size: 13px;
  line-height: 16px;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.greyText7};
  text-transform: lowercase;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  cursor: pointer;
  &:first-child {
    padding-left: 0;
  }
  &:last-child {
    padding-right: 0;
  }
  > span,
  > svg {
    vertical-align: middle;
    display: inline;
  }
`;

const Td = styled.td`
  padding: ${tableCellPadding};
  width: ${({ width }) => width};
  border-bottom: 1px solid ${({ theme }) => theme.colors.orange};
  &:first-child {
    padding-left: 0;
  }

  &:last-child {
    padding-right: 0;
  }
`;

const Tr = styled.tr`
  text-align: left;
  cursor: ${({ onClick }) => onClick ? 'pointer': 'initial'};
`;

// export const StyledTableEmpty = styled('div')(() => ({
//   'text-align': 'center',
//   border: '1px dashed rgba(0,0,0,0.3)',
//   padding: 40,
// }))

const TableRow: React.FC<TRow> = ({ item, columns, onRowClick, rowKey }) => {
  const handleClick = useCallback(() => {
    if (onRowClick) {
      onRowClick(item[rowKey]);
    }
  }, [item, rowKey, onRowClick]);

  const cells = columns.map((column) => (
    <Td key={column.field} width={column.width}>
      {column.value ? column.value(item[column.field], item) : item[column.field]}
    </Td>
  ));

  return <Tr onClick={handleClick}>{cells}</Tr>;
};

const sortingIcons = [null, <Icon.ChevronDown width={16} height={16} />, <Icon.ChevronUp width={16} height={16} />];

const TableHead: React.FC<THead> = ({ column, changeSorting, sortState }) => {
  const handleClick = useCallback(() => {
    changeSorting(column.field);
  }, [column, changeSorting]);

  return (
    <Th key={column.field} onClick={handleClick}>
      <span>{column.header}</span>
      {sortingIcons[sortState]}
    </Th>
  );
};

export const Table: React.FC<TProps> = ({ items, columns, rowKey = 'id', onRowClick }) => {
  const [sorting, setSorting] = useState({ field: '', sort: 0 });
  const changeSorting = useCallback(
    (field: string) => {
      if (sorting.field === field) {
        setSorting({ field, sort: (sorting.sort + 1) % sortingIcons.length });
      } else {
        setSorting({ field, sort: 1 });
      }
    },
    [sorting, setSorting],
  );

  const sortedItems = useMemo(() => {
    if (sorting.field && sorting.sort) {
      return [...items].sort((a, b) => {
        if (a[sorting.field] < b[sorting.field]) {
          return sorting.sort === 1 ? -1 : 1;
        }
        if (a[sorting.field] > b[sorting.field]) {
          return sorting.sort === 1 ? 1 : -1;
        }
        return 0;
      });
    }
    return [...items];
  }, [items, sorting]);

  const headers = columns.map((column) => (
    <TableHead
      column={column}
      sortState={column.field === sorting.field ? sorting.sort : 0}
      key={column.field}
      changeSorting={changeSorting}
    />
  ));

  const rows = sortedItems.map((item) => (
    <TableRow item={item} rowKey={rowKey} key={item[rowKey]} columns={columns} onRowClick={onRowClick} />
  ));

  return (
    <div>
      <InnerTable cellSpacing={0}>
        <thead>
          <Tr>{headers}</Tr>
        </thead>
        <tbody>{rows}</tbody>
      </InnerTable>
    </div>
  );
};
