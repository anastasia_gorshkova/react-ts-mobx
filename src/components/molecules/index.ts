import { Footer } from './Footer';
import { InputGroup } from './InputGroup';
import { Table } from './Table';

export { Footer, InputGroup, Table };
