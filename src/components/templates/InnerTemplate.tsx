import React from 'react';
import styled from '@/theme';
import { Section } from '@/components/atoms';
import { MainTemplate } from './MainTemplate';

export const InnerSection = styled(Section)`
  padding: 40px 72px;
`;

export const InnerTemplate: React.FC = ({ children }) => (
  <MainTemplate>
    <InnerSection>{children}</InnerSection>
  </MainTemplate>
);
