import React from 'react';
import styled from '@/theme';
import { Footer } from '@/components/molecules';

export const MainTemplateWrapper = styled.div`
  min-height: 100%;
  display: flex;
  flex-direction: column;
  width: 1200px;
  margin: 0 auto;
`;

export const MainTemplateContent = styled.div`
  flex: 1;
`;

export const MainTemplate: React.FC = ({ children }) => (
  <MainTemplateWrapper>
    <MainTemplateContent>{children}</MainTemplateContent>
    <Footer />
  </MainTemplateWrapper>
);
