import React from 'react';
import styled from '@/theme';
import { Section, Card } from '@/components/atoms';
import { MainTemplate } from './MainTemplate';

export const InnerSection = styled(Section)`
  padding: 40px 72px 40px 372px;
`;

export const Menu = styled(Card)`
  border-radius: 0px;
  padding: 40px;
  position: fixed;
  left: 0;
  top: 0;
  bottom: 0;
`;

type TProps = {
  menu: React.ReactNode;
};

export const MenuTemplate: React.FC<TProps> = ({ children, menu }) => (
  <MainTemplate>
    <Menu maxWidth={300}>{menu}</Menu>
    <InnerSection>{children}</InnerSection>
  </MainTemplate>
);
