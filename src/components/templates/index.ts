import { MainTemplate } from './MainTemplate';
import { InnerTemplate } from './InnerTemplate';
import { MenuTemplate } from './MenuTemplate';

export { MainTemplate, InnerTemplate, MenuTemplate };
