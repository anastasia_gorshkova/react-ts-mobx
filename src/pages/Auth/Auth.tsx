import React, { useCallback } from 'react';
import { RouteComponentProps } from 'react-router';
import styled from '@emotion/styled';
import { MainTemplate } from '@/components/templates';
import { Section, Card, Input, Title, Button } from '@/components/atoms';

export const AuthSection = styled(Section)`
  min-height: 100%;
  display: flex;
  margin: auto;
`;

export const AuthCard = styled(Card)`
  max-width: 640px;
`;

export const InputWrapper = styled.div`
  margin-bottom: 20px;
  input {
    width: 100%;
  }
`;

export const ButtonWrapper = styled.div`
  margin-top: 36px;
  button {
    width: 100%;
  }
`;

export const Auth: React.FC<RouteComponentProps> = ({ history }) => {
  const login = useCallback(() => history.push('/projects'), [history]);

  return (
    <MainTemplate>
      <AuthSection>
        <AuthCard>
          <Title>Авторизация</Title>
          <InputWrapper>
            <Input placeholder="Логин" />
          </InputWrapper>
          <InputWrapper>
            <Input placeholder="Пароль" type="password" />
          </InputWrapper>
          <ButtonWrapper>
            <Button.Primary onClick={login}>Войти</Button.Primary>
          </ButtonWrapper>
        </AuthCard>
      </AuthSection>
    </MainTemplate>
  );
};
