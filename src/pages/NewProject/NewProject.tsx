import React, { useCallback } from 'react';
import { RouteComponentProps } from 'react-router';
import { InnerTemplate } from '@/components/templates';
import { Title, Button, Input, Textarea } from '@/components/atoms';
import styled from '@emotion/styled';

export const InputWrapper = styled.div`
  margin-bottom: 20px;
  input {
    width: 100%;
  }
`;

export const ButtonWrapper = styled.div`
  margin-top: 36px;
  button {
    margin-left: auto;
    display: block;
  }
`;

export const NewProject: React.FC<RouteComponentProps> = ({ history }) => {
  const create = useCallback(() => history.push('/projects/4'), [history]);
  return (
    <InnerTemplate>
      <Title>Новый проект</Title>
      <InputWrapper>
        <Input placeholder="Название" />
      </InputWrapper>
      <InputWrapper>
        <Textarea placeholder="Описание" rows={4} />
      </InputWrapper>
      <ButtonWrapper>
        <Button.Primary onClick={create}>Создать</Button.Primary>
      </ButtonWrapper>
    </InnerTemplate>
  );
};
