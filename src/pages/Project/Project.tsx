import React from 'react';
import { RouteComponentProps } from 'react-router';
import { MenuTemplate } from '@/components/templates';
import { Icon, Title, InnerLink } from '@/components/atoms';
import styled from '@emotion/styled';
import { TProduct } from '@/store/ProductStore';
import NotFound from '@/pages/NotFound';

export const InputWrapper = styled.div`
  margin-bottom: 20px;
  input {
    width: 100%;
  }
`;

export const ButtonWrapper = styled.div`
  margin-top: 36px;
  button {
    margin-left: auto;
    display: block;
  }
`;

export const Ul = styled.ul`
  list-style: none;
  padding: 0;
`;

export const Li = styled.ul`
  margin-bottom: 16px;
  padding: 0;
  cursor: pointer;
  > a,
  > svg {
    vertical-align: middle;
    display: inline;
  }
  > svg {
    margin-right: 8px;
  }
`;

type TProps = RouteComponentProps & {
  project: TProduct | null;
};

export const Project: React.FC<TProps> = ({ location, project }) =>
  project ? (
    <MenuTemplate
      menu={
        <Ul>
          <Li>
            <Icon.ImageFolder />
            <InnerLink to={`${location.pathname}/assets`}>Ассеты</InnerLink>
          </Li>
          <Li>
            <Icon.CardLayout />
            <InnerLink to={`${location.pathname}/types`}>Типы</InnerLink>
          </Li>
          <Li>
            <Icon.Cards />
            <InnerLink to={`${location.pathname}/cards`}>Карты</InnerLink>
          </Li>
        </Ul>
      }
    >
      <Title>{project.name}</Title>
      <InputWrapper>{project.description}</InputWrapper>
    </MenuTemplate>
  ) : (
    <NotFound />
  );
