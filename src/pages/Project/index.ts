import { observer, inject } from 'mobx-react';
import { ProductStore } from '@/store/ProductStore';
import { Project } from './Project';

export default inject(({ productStore }: { productStore: ProductStore }) => ({
  project: productStore.currentProduct,
}))(observer(Project));
