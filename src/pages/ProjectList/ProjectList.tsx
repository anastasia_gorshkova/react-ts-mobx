import React, { useCallback } from 'react';
import { RouteComponentProps } from 'react-router';
import { InnerTemplate } from '@/components/templates';
import { Title, Button } from '@/components/atoms';
import { Table } from '@/components/molecules';
import styled from '@emotion/styled';
import { TProduct } from '@/store/ProductStore';

export const ButtonWrapper = styled.div`
  margin-top: 36px;
  button {
    margin-left: auto;
    display: block;
  }
`;

const columns = [
  { field: 'name', header: 'Название', width: 200 },
  { field: 'cardsAmmount', header: 'Кол-во карт', width: 110 },
  { field: 'description', header: 'Описание' },
];

type TProps = RouteComponentProps & {
  projects?: TProduct[];
};

export const ProjectList: React.FC<TProps> = ({ history, projects }) => {
  const goTo = useCallback(() => history.push('/new-project'), [history]);
  const handleRowClick = useCallback((id) => history.push(`/projects/${id}`), [history]);
  return (
    <InnerTemplate>
      <Title>Список проектов</Title>
      {projects ? <Table items={projects} columns={columns} onRowClick={handleRowClick} /> : null}
      <ButtonWrapper>
        <Button.Primary onClick={goTo}>Добавить</Button.Primary>
      </ButtonWrapper>
    </InnerTemplate>
  );
};
