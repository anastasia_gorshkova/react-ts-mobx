import { observer, inject } from 'mobx-react';
import { ProductStore } from '@/store/ProductStore';
import { ProjectList } from './ProjectList';

export default inject(({ productStore }: { productStore: ProductStore }) => ({
  projects: productStore.products,
}))(observer(ProjectList));
