import React, { useState, useEffect } from 'react';
import { Switch, Route } from 'react-router-dom';
import Project from '@/pages/Project';
import NotFound from '@/pages/NotFound';
import { RouteComponentProps } from 'react-router';
import { observer, inject } from 'mobx-react';
import { ProductStore } from '@/store/ProductStore';

type TProps = RouteComponentProps<{ id: string }> & {
  isLoading: boolean;
  fetchProduct: (id: number) => void;
};

const Routing: React.FC<TProps> = ({ match, isLoading, fetchProduct }) => {
  const [isReady, setIsReady] = useState(false);
  useEffect(() => {
    setIsReady(true);
    fetchProduct(Number(match.params.id));
  }, []);

  console.log(isLoading);

  return isLoading || !isReady ? (
    <div>Loading</div>
  ) : (
    <Switch>
      <Route exact path={match.path} component={Project} />
      <Route component={NotFound} />
    </Switch>
  );
};

export default inject(({ productStore }: { productStore: ProductStore }) => ({
  isLoading: productStore.isProductLoading,
  fetchProduct: productStore.fetchProduct,
}))(observer(Routing));
