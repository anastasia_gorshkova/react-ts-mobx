import React, { useEffect, useState } from 'react';
import { Switch, Route } from 'react-router-dom';
import ProjectList from '@/pages/ProjectList';
import NotFound from '@/pages/NotFound';
import { RouteComponentProps } from 'react-router';
import { observer, inject } from 'mobx-react';
import { ProductStore } from '@/store/ProductStore';
import CurrentProject from './CurrentProject';

type TProps = RouteComponentProps & {
  isLoading: boolean;
  fetchProducts: () => void;
};

const Routing: React.FC<TProps> = ({ match, isLoading, fetchProducts }) => {
  const [isReady, setIsReady] = useState(false);
  useEffect(() => {
    fetchProducts();
    setIsReady(true);
  }, []);

  return isLoading || !isReady ? (
    <div>Loading</div>
  ) : (
    <Switch>
      <Route exact path={match.path} component={ProjectList} />
      <Route path={`${match.path}/:id`} component={CurrentProject} />
      <Route component={NotFound} />
    </Switch>
  );
};

export default inject(({ productStore }: { productStore: ProductStore }) => ({
  isLoading: productStore.isLoading,
  fetchProducts: productStore.fetchProducts,
}))(observer(Routing));
