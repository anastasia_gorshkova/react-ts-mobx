import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Auth from '@/pages/Auth';
import NewProject from '@/pages/NewProject';
import NotFound from '@/pages/NotFound';
import Projects from './Projects';

const Routing: React.FC = () => (
  <Switch>
    <Redirect exact from="/" to="/login" />
    <Route exact path="/login" component={Auth} />
    {/* <Route exact path="/profile" component={Profile} /> */}
    <Route path="/projects" component={Projects} />
    <Route exact path="/new-project" component={NewProject} />
    <Route component={NotFound} />
  </Switch>
);

export default Routing;
