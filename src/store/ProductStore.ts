import { observable, runInAction } from 'mobx';
import axios from 'axios';

export type TProduct = {
  id: number;
  name: string;
  cardsAmmount: number;
  description: string;
};

export class ProductStore {
  @observable
  public products: TProduct[] = [];
  @observable
  public currentProduct: TProduct | null = null;
  @observable
  public isLoading: boolean = false;
  @observable
  public isProductLoading: boolean = false;

  fetchProducts = async () => {
    runInAction(() => {
      this.isLoading = true;
    });
    const { data: products } = await axios.get('/api/projects');
    runInAction(() => {
      this.isLoading = false;
      this.products = products;
    });
  };

  fetchProduct = async (id: number) => {
    runInAction(() => {
      this.isProductLoading = true;
    });
    const { data: currentProduct } = await axios.get(`/api/projects/${id}`);
    runInAction(() => {
      this.isProductLoading = false;
      this.currentProduct = currentProduct;
    });
  };
}
