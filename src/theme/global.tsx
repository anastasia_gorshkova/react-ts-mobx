import React from 'react';
import { Global, css } from '@emotion/core';
import emotionNormalize from 'emotion-normalize';
import { theme } from './theme';

export const GlobalStyles: React.FC = () => (
  <Global
    styles={css`
      ${emotionNormalize}
      #root {
        min-height: 100%;
        display: flex;
      }
      html,
      body {
        height: 100%;
        padding: 0;
        margin: 0;
        background: ${theme.colors.bg};
        font-family: 'Roboto', sans-serif;
        color: ${theme.colors.yellowText14Light};
        font-size: 18px;
        line-height: 1.2;
        * {
          box-sizing: border-box;
        }
      }
    `}
  />
);
