import styled, { CreateStyled } from '@emotion/styled';
import { theme, TTheme } from './theme';
import { GlobalStyles } from './global';

export { theme, GlobalStyles };

export default styled as CreateStyled<TTheme>;
