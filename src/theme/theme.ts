export type TTheme = {
  colors: {
    bg: string;
    bg1: string;
    greyText3: string;
    greyText7: string;
    greyText14: string;
    yellowText14Light: string;
    purpleText7: string;
    primary: string;
    orange: string;
    success: string;
    error: string;
  };
  width: number;
};

export const theme: TTheme = {
  colors: {
    bg: '#201830',
    bg1: '#31254A',
    greyText3: '#6B6773',
    greyText7: '#ACAAB3',
    greyText14: '#EAE9F0',
    yellowText14Light: '#FFFBBF',
    purpleText7: '#B0A3CC',
    primary: '#FFF000',
    orange: '#F08C00',
    success: '#009944',
    error: '#cf000f',
  },
  width: 1280,
};
